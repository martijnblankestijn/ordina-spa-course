package nl.single.control;

import nl.single.domain.Category;
import nl.single.domain.Dish;

import javax.ws.rs.WebApplicationException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

public class MenuService {
  public List<Category> getCategories(String code, LocalDate date) {
    Objects.requireNonNull(date, "Date is required parameter, did YOU convert (@PathParam)");

    if(!"APICIUS".equals(code)) return emptyList();

    if(LocalDate.now().equals(date)) {
      return asList(
          createCategory("entrees", "Entrees", asList(new Dish("Hollandse Garnalencocktail", 10.5, "Fijne hollands garnalen"))),
          createCategory("main", "Main courses", asList(new Dish("Roast Beef", 29, "Roast beef direct van onze keurslager, 200grams"))),
          createCategory("dessert", "Dessert", asList(new Dish("Tiramisu", 29, "Italiaans godendessert"))));
    }
    else {
      return asList(
          createCategory("amuses", "Amuses", asList(new Dish("Coquilles afgeblust met pastis", 10.5, "Prima"))),
          createCategory("entrees", "Entrees", asList(new Dish("Hollandse Garnalencocktail", 10.5, "Fijne hollands garnalen"))),
          createCategory("main", "Main courses", asList(new Dish("Roast Beef", 29, "Roast beef direct van onze keurslager, 200grams"))),
          createCategory("dessert", "Dessert", asList(new Dish("Tiramisu", 29, "Italiaans godendessert"))));
    }
  }

  private Category createCategory(String code, String name, List<Dish> dishes) {
    Category main = new Category(code);
    main.setName(name);
    main.setDishes(dishes);
    return main;
  }
}
