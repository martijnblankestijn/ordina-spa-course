package nl.single.boundary;

import nl.single.control.MenuService;
import nl.single.domain.Category;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.time.LocalDate;
import java.util.List;

import static javax.json.Json.createObjectBuilder;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("restaurants/{code}/menu")
public class MenuResource {
  private MenuService service = new MenuService();

  @Context UriInfo uriInfo;


  @GET @Produces(APPLICATION_JSON)
  public Response getMenu(@PathParam("code") String code) {

    LocalDate date = LocalDate.now();

    List<Category> categories = service.getCategories(code, date);

    JsonArrayBuilder builder = categories
        .stream()
        .map(this::mapCategory)
        .collect(Json::createArrayBuilder,
            JsonArrayBuilder::add,
            JsonArrayBuilder::add);

    return Response.ok()
        .entity(createObjectBuilder()
            .add("date", "2014-09-02")
            .add("categories", builder)
            .build())
        .build();
  }

  private JsonObjectBuilder mapCategory(Category c) {
    return createObjectBuilder()
        .add("name", c.getName())
        .add("href", buildLink(c));
  }

  private String buildLink(Category c) {
    return uriInfo.getAbsolutePathBuilder()
        .path("categories/{category}")
        .resolveTemplate("category", c.getCode())
        .build().toString();
  }

}
