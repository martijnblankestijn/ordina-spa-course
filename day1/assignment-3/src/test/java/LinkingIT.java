import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static junit.framework.TestCase.assertEquals;

public class LinkingIT {
  private Client client;

  @Before
  public void setup() {
    client = ClientBuilder.newBuilder()
        .register(new LoggingFilter(Logger.getLogger("TEST"), true))
        .build();
  }

  @Test
  public void getMenuForTodayWithReservationLink() throws Exception {
    Response response = client.target("http://localhost:8080/assignment-3/restaurants/{restaurantCode}/menu")
        .resolveTemplate("restaurantCode", "APICIUS")
        .request(APPLICATION_JSON_TYPE)
        .get();

    assertEquals(200, response.getStatus());
    String expectedLink = "http://localhost:8080/assignment-3/reservation/" + LocalDate.now();
    assertEquals(expectedLink, response.getLink("reserve").getUri().toString());
  }

  @Test
  public void getMenuForFixedDayWithReservationLink() throws Exception {
    Response response = client.target("http://localhost:8080/assignment-3/restaurants/{restaurantCode}/menu/2015-04-18")
        .resolveTemplate("restaurantCode", "APICIUS")
        .request(APPLICATION_JSON_TYPE)
        .get();

    assertEquals(200, response.getStatus());
    Link link = response.getLink("reserve");
    // imagine this to be the next client-call! No need to know the exact url in advance (HATEOAS)
    assertEquals("http://localhost:8080/assignment-3/reservation/2015-04-18", link.getUri().toString());
  }

}
