import nl.single.domain.Category;
import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import static java.util.Arrays.asList;
import static javax.ws.rs.core.MediaType.*;
import static javax.ws.rs.core.Response.Status.NOT_ACCEPTABLE;
import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MenuResourceIT {
  private Client client;

  @Before
  public void setup() {
    client = ClientBuilder.newBuilder()
        .register(new LoggingFilter(Logger.getLogger("TEST"), true))
        .build();
  }

  @Test
  public void getMenuWithoutDate() throws Exception {
    Response response = client.target("http://localhost:8080/assignment-3/restaurants/{restaurantCode}/menu")
        .resolveTemplate("restaurantCode", "APICIUS")
        .request(APPLICATION_JSON_TYPE)
        .get();

    assertEquals(200, response.getStatus());

    JsonObject jsonObject = response.readEntity(JsonObject.class);
    JsonArray categories = jsonObject.getJsonArray("categories");

    assertEquals("Weet je zeker dat deze resource methode de datum van vandaag levert?", 3, categories.size());

    JsonObject obj = (JsonObject) categories.get(0);
    assertEquals("http://localhost:8080/assignment-3/restaurants/APICIUS/menu/categories/entrees", obj.getString("href"));
  }

  @Test
  public void getMenuForNonExistingRestaurant() throws Exception {
    Response response = client.target("http://localhost:8080/assignment-3/restaurants/{restaurantCode}/menu/2015-04-18")
        .resolveTemplate("restaurantCode", "BESTAATNIET")
        .request(APPLICATION_JSON_TYPE)
        .get();

    assertEquals(404, response.getStatus());

  }

  @Test
  public void getMenuWithDate() throws Exception {
    Response response = client.target("http://localhost:8080/assignment-3/restaurants/{restaurantCode}/menu/2015-04-18")
        .resolveTemplate("restaurantCode", "APICIUS")
        .request(APPLICATION_JSON_TYPE)
        .get();

    assertEquals(200, response.getStatus());

    JsonObject jsonObject = response.readEntity(JsonObject.class);
    JsonArray categories = jsonObject.getJsonArray("categories");

    assertEquals("Weet je zeker dat je de nieuwe resource methode  heb gemaakt?", 4, categories.size());
  }

  @Test
  public void getMenuWithCacheControl() throws Exception {
    Response response = client.target("http://localhost:8080/assignment-3/restaurants/{restaurantCode}/menu/2015-04-18")
        .resolveTemplate("restaurantCode", "APICIUS")
        .request(APPLICATION_JSON_TYPE)
        .get();

    assertEquals(200, response.getStatus());
    JsonObject jsonObject = response.readEntity(JsonObject.class);
    JsonArray categories = jsonObject.getJsonArray("categories");

    assertEquals("Weet je zeker dat je de nieuwe resource methode  heb gemaakt?", 4, categories.size());
  }

}
