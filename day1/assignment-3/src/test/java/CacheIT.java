import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

public class CacheIT {
  private Client client;

  @Before
  public void setup() {
    client = ClientBuilder.newBuilder()
        .register(new LoggingFilter(Logger.getLogger("TEST"), true))
        .build();
  }

  @Test
  public void checkCacheControl() throws Exception {
    Response response = client.target("http://localhost:8080/assignment-3/restaurants/{restaurantCode}/menu/2015-04-18")
        .resolveTemplate("restaurantCode", "APICIUS")
        .request(APPLICATION_JSON_TYPE)
        .get();

    assertEquals(200, response.getStatus());
    CacheControl cacheControl = CacheControl.valueOf(response.getHeaderString("Cache-Control"));

    assertEquals(3600, cacheControl.getMaxAge());
    assertFalse(cacheControl.isPrivate());
  }

}
