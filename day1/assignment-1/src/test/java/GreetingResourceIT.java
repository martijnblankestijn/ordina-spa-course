import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static junit.framework.TestCase.assertEquals;

public class GreetingResourceIT {

  private static final String REST_URL = "http://localhost:8080/assignment-1/";
  private static final Logger TEST = Logger.getLogger("TEST");

  private static final String DEFAULT_GREETING = "Hi";

  @Before
  public void reloadJavaUtilLoggingConfigurationWithMyOwn() throws IOException {
    InputStream ins = GreetingResourceIT.class.getResourceAsStream("/logging.properties");
    LogManager.getLogManager().readConfiguration(ins);
    TEST.info("++++++++++++++++   STARTING NEW TEST       ++++++++++++++++");
    TEST.info("-----------------------------------------------------------");
    
    HttpURLConnection httpConnection = openHttpUrlConnection("greetings");
    httpConnection.setRequestMethod("DELETE");

    assertEquals("Before, impossible to clear the greetings", 204, httpConnection.getResponseCode());
  }



  @Test
  public void allGreetingsEmpty() throws Exception {
    HttpURLConnection httpConnection = openHttpUrlConnection("greetings");
    httpConnection.setRequestMethod("GET");

    assertEquals(200, httpConnection.getResponseCode());
  }

  @Test
  public void retrieveAbsentGreeting() throws Exception {
    HttpURLConnection httpConnection = openHttpUrlConnection("greetings/" + DEFAULT_GREETING);
    httpConnection.setRequestMethod("GET");

    assertEquals(404, httpConnection.getResponseCode());
  }

  
  @Test
  public void addGreeting() throws Exception {
    HttpURLConnection httpConnection = openHttpUrlConnection("greetings/" + DEFAULT_GREETING);
    httpConnection.setRequestMethod("PUT");
    assertEquals("PUT did NOT succeed!!", 204, httpConnection.getResponseCode());

    // ASSERT that the greeting is add to /greetings resource
    httpConnection = openHttpUrlConnection("greetings");
    httpConnection.setRequestMethod("GET");
    
    assertEquals(200, httpConnection.getResponseCode());
    assertEquals(DEFAULT_GREETING, getInputFromHttpConnection(httpConnection));

      // ASSERT that the greeting is retrievable from 'greeting/Hi'
    final String retrieveUrl = "greetings/" + DEFAULT_GREETING;
    httpConnection = openHttpUrlConnection(retrieveUrl);
    httpConnection.setRequestMethod("GET");
    
    assertEquals("GET of " + retrieveUrl + " did not succeed.", 200, httpConnection.getResponseCode());
    assertEquals(DEFAULT_GREETING, getInputFromHttpConnection(httpConnection));

  }

  @Test
  public void testGreetWithNonExistingGreet() throws Exception {
    HttpURLConnection httpConnection = openHttpUrlConnection("greetings/" + DEFAULT_GREETING + "/Maestro");
    httpConnection.setRequestMethod("POST");

    assertEquals(404, httpConnection.getResponseCode());
  }

  
    @Test
  public void greet() throws Exception {
          HttpURLConnection httpConnection = openHttpUrlConnection("greetings/" + DEFAULT_GREETING);
    httpConnection.setRequestMethod("PUT");

    assertEquals(204, httpConnection.getResponseCode());

    httpConnection = openHttpUrlConnection("greetings/" + DEFAULT_GREETING + "/Maestro");
    httpConnection.setRequestMethod("POST");
    
    assertEquals(200, httpConnection.getResponseCode());
    assertEquals("Hi Maestro", getInputFromHttpConnection(httpConnection));

  }

    @Test
  public void deleteNonExistingGreeting() throws Exception {
    HttpURLConnection httpConnection = openHttpUrlConnection("greetings/" + DEFAULT_GREETING);
    httpConnection.setRequestMethod("DELETE");
    
    assertEquals(204, httpConnection.getResponseCode());
  }

  
  @Test
  public void createAndDeleteAGreeting() throws Exception {
    HttpURLConnection httpConnection = openHttpUrlConnection("greetings/" + DEFAULT_GREETING);
    httpConnection.setRequestMethod("PUT");

    assertEquals(204, httpConnection.getResponseCode());

    httpConnection = openHttpUrlConnection("greetings/" + DEFAULT_GREETING);
    httpConnection.setRequestMethod("DELETE");
    assertEquals(204, httpConnection.getResponseCode());

    httpConnection = openHttpUrlConnection("greetings/");
    httpConnection.setRequestMethod("GET");
    assertEquals("", getInputFromHttpConnection(httpConnection));

  }
  
    @Test
  public void addMultipleGreetings() throws Exception {
    HttpURLConnection httpConnection = openHttpUrlConnection("greetings/" + DEFAULT_GREETING);
    httpConnection.setRequestMethod("PUT");
    assertEquals("PUT did NOT succeed!!", 204, httpConnection.getResponseCode());
    
    httpConnection = openHttpUrlConnection("greetings/" + "Hello");
    httpConnection.setRequestMethod("PUT");
    assertEquals("PUT did NOT succeed!!", 204, httpConnection.getResponseCode());
    
    httpConnection = openHttpUrlConnection("greetings/" + "GoodDay");
    httpConnection.setRequestMethod("PUT");
    assertEquals("PUT did NOT succeed!!", 204, httpConnection.getResponseCode());

    httpConnection = openHttpUrlConnection("greetings/");
    httpConnection.setRequestMethod("GET");
    assertEquals("GoodDay,Hello,Hi", getInputFromHttpConnection(httpConnection));

  }


  private HttpURLConnection openHttpUrlConnection(final String relativeUrl) throws IOException {
    String url = REST_URL + relativeUrl;
    TEST.info("URL TO USE: " + url);
    return (HttpURLConnection) new URL(url).openConnection();
  }

  private String getInputFromHttpConnection(HttpURLConnection httpConnection) throws IOException {
    BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
        (httpConnection.getInputStream())));

    StringBuilder builder = new StringBuilder();
    String output;

    while ((output = responseBuffer.readLine()) != null) {
      builder.append(output);
    }
    return builder.toString();
  }
}
