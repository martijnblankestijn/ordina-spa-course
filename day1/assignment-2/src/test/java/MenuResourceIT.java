import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.Before;
import org.junit.Test;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import static java.util.Arrays.asList;
import static javax.ws.rs.core.MediaType.*;
import static javax.ws.rs.core.Response.Status.NOT_ACCEPTABLE;
import static junit.framework.TestCase.assertEquals;

public class MenuResourceIT {
  private Client client;

  @Before
  public void setup() {
    client = ClientBuilder.newBuilder()
        .register(new LoggingFilter(Logger.getLogger("TEST"), true))
        .build();
  }


  @Test
  public void getMenuInJson() throws Exception {
    Response response = client.target("http://localhost:8080/assignment-2/restaurants/{restaurantCode}/menu")
        .resolveTemplate("restaurantCode", "APICIUS")
        .request(APPLICATION_JSON_TYPE)
        .get();

    assertEquals(200, response.getStatus());

    JsonObject jsonObject = response.readEntity(JsonObject.class);
    JsonArray categories = jsonObject.getJsonArray("categories");

    JsonObject obj = (JsonObject) categories.get(0);
    assertEquals("http://localhost:8080/assignment-2/restaurants/APICIUS/menu/categories/entrees", obj.getString("href"));
  }


  @Test
  public void getMenuNotAcceptableMediaTypes() throws Exception {
    List<MediaType> mediaTypes =
        asList(APPLICATION_ATOM_XML_TYPE,
            APPLICATION_XML_TYPE,
            TEXT_PLAIN_TYPE,
            TEXT_XML_TYPE);

    // Assert for all media Types the Media Type is not acceptable for the server.
    mediaTypes.stream().forEach(m -> assertEquals(NOT_ACCEPTABLE.getStatusCode(), tryResourceWithMediaType(m).getStatus()));
  }

  private Response tryResourceWithMediaType(MediaType mediaTypes) {
    return client.target("http://localhost:8080/assignment-2/restaurants/{restaurantCode}/menu")
        .resolveTemplate("restaurantCode", "APICIUS")
        .request()
        .accept(mediaTypes)
        .get();
  }

}
