package optional;

import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.Before;
import org.junit.Test;

import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static junit.framework.TestCase.assertEquals;

public class MenuWithTemplateLinkIT {
  private Client client;

  @Before
  public void setup() {
    client = ClientBuilder.newBuilder()
        .register(new LoggingFilter(Logger.getLogger("TEST"), true))
        .build();
  }


  @Test
  public void getMenu() throws Exception {
    // See https://jersey.java.net/documentation/latest/monitoring_tracing.html#tracing
    Response response = client.target("http://localhost:8080/assignment-2/restaurants/{restaurantCode}/menu")
        .resolveTemplate("restaurantCode", "APICIUS")
        .request("application/vnd.api+json")
        .get();
    assertEquals(200, response.getStatus());

    JsonObject jsonObject = response.readEntity(JsonObject.class);

    WebTarget target = client.target(
        jsonObject
            .getJsonObject("links")
            .getString("categories.category"));

    jsonObject.getJsonArray("categories").stream()
        .map(v -> (JsonObject) v)
        .map(obj -> target
            .resolveTemplate("categories.category",
                obj.getJsonObject("links")
                    .getString("category"))
            .request(APPLICATION_JSON_TYPE)
            .get())
        .forEach(r -> assertEquals(200, r.getStatus()));
  }

}
