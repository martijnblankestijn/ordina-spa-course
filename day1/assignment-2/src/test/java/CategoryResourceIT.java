import nl.single.domain.Category;
import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.Before;
import org.junit.Test;

import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.MediaType.APPLICATION_XML_TYPE;
import static junit.framework.TestCase.assertEquals;

public class CategoryResourceIT {
  private Client client;

  @Before
  public void setup() {
    client = ClientBuilder.newBuilder()
        .register(new LoggingFilter(Logger.getLogger("TEST"), true))
        .build();
  }


  @Test
  public void getMenu() throws Exception {
    Response response = client.target("http://localhost:8080/assignment-2/restaurants/{restaurantCode}/menu")
        .resolveTemplate("restaurantCode", "APICIUS")
        .request(APPLICATION_JSON_TYPE)
        .get();

    String link =
        ((JsonObject) response
            .readEntity(JsonObject.class)
            .getJsonArray("categories")
            .get(0))
            .getString("href");

    WebTarget target = client.target(link);

    // check that it can be marshalled as JSON
    assertEquals(200, target.request(APPLICATION_JSON_TYPE).get().getStatus());

    // and as XML
    Response categoryResponse = target.request(APPLICATION_XML).get();
    assertEquals(200, categoryResponse.getStatus());
    assertEquals(APPLICATION_XML_TYPE, categoryResponse.getMediaType());

    Category category = categoryResponse.readEntity(Category.class);
    assertEquals(1, category.getDishes().size());
  }
}
