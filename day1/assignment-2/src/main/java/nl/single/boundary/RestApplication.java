package nl.single.boundary;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static java.util.logging.Level.INFO;

@ApplicationPath("")
public class RestApplication extends Application {
  private final static Logger LOG = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

  public RestApplication() {
    LOG.log(INFO, "Initializing RestApplication {0}", RestApplication.class);
  }

  @Override
  public Map<String, Object> getProperties() {
    LOG.log(INFO, "getProperties called");
    HashMap<String, Object> map = new HashMap<>(super.getProperties());
    // See https://jersey.java.net/documentation/latest/monitoring_tracing.html#tracing
    // do NOT use in PRODUCTION
    map.put("jersey.config.server.tracing.type",
            "ALL"); // Values( ALL, OFF, ON_DEMAND )
    map.put("jersey.config.server.tracing.threshold",
            "VERBOSE"); // Values( SUMMARY, TRACE, VERBOSE )
    return map;
  }
}
