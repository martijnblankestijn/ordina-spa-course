package nl.single.control;

import nl.single.domain.Category;
import nl.single.domain.Dish;

import javax.ws.rs.WebApplicationException;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

public class MenuService {

  public List<Category> getCategories(String code) {
    if(!"APICIUS".equals(code)) return emptyList();

    Category entrees = new Category("entrees");
    entrees.setName("Entrees");
    entrees.setDishes(asList(new Dish("Hollandse Garnalencocktail", 10.5, "Fijne hollands garnalen")));

    Category main = new Category("main");
    main.setName("Main courses");
    main.setDishes(asList(new Dish("Roast Beef", 29, "Roast beef direct van onze keurslager, 200grams")));

    Category dessert = new Category("dessert");
    dessert.setName("Dessert");
    dessert.setDishes(asList(new Dish("Tiramisu", 29, "Italiaans godendessert")));

    return asList(entrees, main, dessert);
  }

  public Category getCategory(String restaurantCode, String categoryCode) {
    return getCategories(restaurantCode).stream()
        .filter(c -> c.getCode().equals(categoryCode))
        .findFirst()
        .orElseThrow(() -> new WebApplicationException(NOT_FOUND));
  }
}
