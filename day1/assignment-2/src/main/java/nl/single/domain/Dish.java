package nl.single.domain;

public class Dish {
  private String name;
  private double price;
  private String description;

  public Dish() {}

  public Dish(String name, double price, String description) {
    this.name = name;
    this.price = price;
    this.description = description;
  }

  @Override public String toString() {
    return "Dish{" +
        "name='" + name + '\'' +
        ", price=" + price +
        ", description='" + description + '\'' +
        '}';
  }
}
