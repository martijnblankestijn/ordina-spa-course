package nl.single.domain;

import java.util.List;

public class Category {
  private String code;
  private String name;
  private List<Dish> dishes;

  public Category() {}
  public Category(String code) {
    this.code = code;
  }


  public void setName(String name) {
    this.name = name;
  }

  public void setDishes(List<Dish> dishes) {
    this.dishes = dishes;
  }

  public String getName() {
    return name;
  }

  public String getCode() {
    return code;
  }

  public List<Dish> getDishes() {
    return dishes;
  }

  @Override public String toString() {
    return "Category{" +
        "code='" + code + '\'' +
        ", name='" + name + '\'' +
        '}';
  }
}
