"use strict";

var adminApp = angular.module("diningAdmin", ["ngRoute", "ngResource"]);

adminApp.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "/partials/admin/dishes.html",
            controller: "DishesController"
        })
        // Step 2 Add a new dish
        // Add a route which uses the NewDishController and navigate the user to the 'edit.html'

        // Step 8 Edit a dish
        // Add a route which uses the EditDishController and navigate the user to the 'edit.html'
        // Optionally use the resolve-,method to query for the types and styles.

        .otherwise({
            redirectTo: '/'
        })
    ;
});

adminApp.value('ServicesBaseUrl', 'http://localhost:8080/backend/')
    // for now only administration for Apicius is supported
    .value('RestaurantCode', 'APICIUS');

adminApp.factory('Dishes', function ($resource, ServicesBaseUrl, RestaurantCode) {
    // Step 8 Edit a dish
    // Make the Resource do a HTTP PUT in case of an update.
    return $resource(ServicesBaseUrl + "restaurants/:restaurantCode/dishes/:id",
        {restaurantCode: RestaurantCode}
    );
});

// Step 9 Use EnumerationResource
// Use factory to create the Enumerations Resource with the 'code' as path parameter.

adminApp.controller("DishesController", function ($scope) {
    // Step 1 Show list of dishes
    // use Dishes Resource to get all dishes for the restaurant.

    // Step 7 Delete a dish
    // create a function for the front-end to remove a dish through HTTP DELETE
});

adminApp.controller("EditDishController", function ($scope) {
    $scope.title = 'Edit an existing Dish';

    // Step 9 Use the Enumeration Resource
    // Replace these will calls to the Enumerations-Resource
    // And if that does not work as you expect think about it and look at the comment in the routeProvider with respect to editing.
    $scope.types = ["MAIN", "STARTER", "DESSERT"];
    $scope.styles = ["DUTCH", "FRENCH", "ITALIAN", "FUSION", "ASIAN"];


    // Step 8 Edit a dish
    // Use the Dishes-Resource to retrieve the dish that is selected

    // Step 8 Edit a dish
    // Update the dish with a HTTP PUT
    //   If the HTTP call is successful use $location to go to the list path ('/') otherwise alert the user
});

adminApp.controller("NewDishController", function ($scope) {
    $scope.title = 'Create a new Dish';

    // Step 9 Use the Enumeration Resource
    // Replace these will calls to the Enumerations-Resource.
    $scope.types = ["MAIN", "STARTER", "DESSERT"];
    $scope.styles = ["DUTCH", "FRENCH", "ITALIAN", "FUSION", "ASIAN"];


    // Step 2 Add a dish
    // Save the new Dish through the Dishes Resource with HTTP POST.
    // If the HTTP call is successful use $location to go to the list path ('/') otherwise alert the user
});
