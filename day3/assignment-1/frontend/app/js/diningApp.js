'use strict'

var diningApp = angular.module('dining', ['ngResource', 'ngRoute', 'ui.bootstrap']);

// Step 6: Make reservation
// DO NOT CHANGE THIS FACTORY
// You can use (read: inject) it to format a date object and check whether something is a date
diningApp.factory('DateFormatter', function () {
    var filler = function (someNumber) {
        return someNumber.toString().length < 2 ? '0' + someNumber : someNumber;
    }

    var formatter = {};
    formatter.format = function (date) {
        return date.getFullYear() + "-" + filler(date.getMonth() + 1) + "-" + filler(date.getDate());
    };

    formatter.isDate = function (supposinglyADate) {
        return (supposinglyADate !== undefined && supposinglyADate !== null);
    };

    return formatter;
});

diningApp.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "partials/frontpage.html",
            controller: "RestaurantsController"
        })
        // Step 4: Show details of restaurant
        // There should be a route to the partials/restaurant.html page. This page shows the details of a single restaurant

        .otherwise({
            redirectTo: '/'
        });
});

diningApp.value('ServicesBaseUrl', 'http://localhost:8080/backend/');

diningApp
    .factory("Restaurants", function($resource, ServicesBaseUrl) {
        // Step 3: Show the lists of restaurants
        // Return a resource to the list of restaurants.
        // The restaurants are accessible under ServiceBaseUrl/restaurants

        // Step 4: Show details of restaurant
        // Hint; you can extend the resource of Step 3 so that it also can be used to retrieve a single restaurant
    })

    // Step 5: Show the dishes of a restaurant
    // Add a resource that returns the list of dishes of a restaurant
    // The dishes should be reachable via ServiceBaseUrl/restaurants/{code}/dishes

    // Step 6: Make reservation
    // Add a resource that returns the number of available seats on a given date and can be used to actually make (POST)
    // a reservation.
    // The url is: ServiceBaseUrl/restaurants/{code}/{date}/reservations

    // Step 10: Show a carousel of featured restaurants
    // Add a resource that returns the list of featured restaurants
    // The url is: ServiceBaseUrl/restaurants/featured

    ;

diningApp.controller('RestaurantsController', function ($scope, Restaurants /* More services should be injected (for step 10) */) {
    // Step 3: Show the lists of restaurants
    // Get the list of restaurants;

    // Step 10: Show the featured restaurants
    // Get the list of currently featured restaurants
});

diningApp.controller("RestaurantController", function ($scope, DateFormatter, Restaurants /* More services should be injected */) {
    // Step 4: Show details of restaurant
    // Get the specific restaurant

    // Step 5: Show the dishes of a restaurant
    // Get the list of dishes of the restaurant

    // Step 6: Make reservation
    // A number of functions are needed to check whether it is possible to make a reservation and to make the actual reservation
    // Below are some hints of the functions that are probably needed but you are of course free to implement the feature
    // as you wish!
    // Make use of the date functions in the DateFormatter to manipulate the JavaScript selected date object.

    // - checkAvailability(): Should check whether it is possible to make a reservation for a selected date and a selected nr of people
    // - makeReservation(): Make the actual reservation on the selected date and for the selected nr of people
    // - isDateSelected(), isNrOfPersonsFilled(): Checks to see whether the needed properties are filled

    // Extra hint: checkAvailability() should be called automatically when both the date is selected and the nr of people is filled in.

    // DO NOT CHANGE THE FUNCTION BELOW OTHERWISE THE DATE PICKER WON'T SHOW ANYMORE
    $scope.showDatePopup = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.showingDatePopup = true;
    };
});

diningApp.controller('RestaurantSearchController', function ($scope, Restaurants /* more services should be injected */) {
    // Step 11: Search a restaurant

    // Use the ui.bootstrap typeahead directive to implement this feature.
    // see http://angular-ui.github.io/bootstrap/#/typeahead
    // Add functions to go to the selected restaurant detail page
});