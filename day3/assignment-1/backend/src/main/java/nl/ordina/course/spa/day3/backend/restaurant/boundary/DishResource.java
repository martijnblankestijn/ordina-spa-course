package nl.ordina.course.spa.day3.backend.restaurant.boundary;

import nl.ordina.course.spa.day3.backend.restaurant.control.DishService;

import javax.ws.rs.*;

@Path("restaurants/{code}/dishes")
public class DishResource {
  private DishService service = new DishService();

  // Step 1: Show the dishes of a restaurant
  // Use HTTP GET to return a list of dishes of a specific (code) restaurant. Use the DishService.;

  // Step 2 Create a dish
  // Use HTTP POST and use the DishService to add a new dish to the restaurant
  // Return a CREATED Response (201) with the url of this Resource with the path <id of the dish> added to it
  // Remember UriInfo ??

  // Step 7 Delete a dish
  // Use HTTP DELETE and use the DishService to remove the dish

  // Step 8 Edit a dish
  // Use HTTP GET and the DishService to return the Dish with the specified id. Return a 404 when the dish is not found.
  // Use HTTP PUT and the DishService to update the Dish with the specified id.
}
