package nl.ordina.course.spa.day3.backend.restaurant.boundary;

import nl.ordina.course.spa.day3.backend.restaurant.entity.CourseType;
import nl.ordina.course.spa.day3.backend.restaurant.entity.CuisineStyle;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonValue;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.Map;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Stream.of;

@Path("enumerations")
public class EnumerationResource {

  /** The supported enumerations which can be queried based on the simple name.
   * Map of key(simpleName of enumeration) and value (Class of enumeration).
   */
  private static final Map<String, Class<? extends Enum>> SUPPORTED_ENUMERATIONS =
      of(CuisineStyle.class,
          CourseType.class
      ).collect(toMap(Class::getSimpleName, identity()));

  //
  // Step 9 Make Enumerations cacheable for an hour
  // Use "no-transform,public,max-age=3600" for the Response-Header "Cache-Control".
  //
  @GET @Path("{enum}")
  public JsonValue getValues(@PathParam("enum") String enumClass) {
    Class<? extends Enum> aClass = SUPPORTED_ENUMERATIONS.get(enumClass);

    // Security check, only let acceptable enumerations be queried.
    if (aClass == null) throw new NotFoundException(enumClass);
    else return of(aClass.getEnumConstants())
        .map(Enum::name)
        .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
        .build();
  }
}
