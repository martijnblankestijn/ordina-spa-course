package nl.ordina.course.spa.day3.backend.restaurant.boundary;

import nl.ordina.course.spa.day3.backend.restaurant.control.RestaurantService;
import nl.ordina.course.spa.day3.backend.restaurant.entity.FeaturedRestaurant;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;

@Path("featured")
public class FeaturedRestaurantsResource {
    private RestaurantService restaurantService = new RestaurantService();

    /**
     * Step 10: Show the featured restaurants
     * Return the list of featured restaurants. The method should be a HTTP GET.
     * Use the RestaurantService to get the lists of featured restaurants
     */
}
