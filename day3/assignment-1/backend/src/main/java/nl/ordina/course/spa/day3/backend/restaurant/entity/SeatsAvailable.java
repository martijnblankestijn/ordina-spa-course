package nl.ordina.course.spa.day3.backend.restaurant.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SeatsAvailable {
    private int seatsAvailable;

    public SeatsAvailable() { }

    public SeatsAvailable(int seatsAvailable) {
        this.seatsAvailable = seatsAvailable;
    }
}
