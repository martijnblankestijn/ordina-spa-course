package nl.ordina.course.spa.day3.backend.restaurant.entity;

public enum CourseType {
  MAIN, STARTER, DESSERT
}
