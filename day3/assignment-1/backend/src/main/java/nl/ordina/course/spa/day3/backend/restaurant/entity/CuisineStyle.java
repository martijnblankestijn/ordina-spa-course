package nl.ordina.course.spa.day3.backend.restaurant.entity;

public enum CuisineStyle {
    DUTCH,
    FRENCH,
    ITALIAN,
    FUSION,
    ASIAN;
}
