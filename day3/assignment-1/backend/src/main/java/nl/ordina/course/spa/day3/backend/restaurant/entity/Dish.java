package nl.ordina.course.spa.day3.backend.restaurant.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

@XmlRootElement
@XmlAccessorType(FIELD)
public class Dish {
    private Long id;
    private String name;
    private BigDecimal price;
    private boolean vegetarian;
    private CourseType type;
    private CuisineStyle style;
    private String description;
    private String imagePath;

    public Dish() {
    }

    public Dish(Long id, String name, BigDecimal price, boolean vegetarian, CourseType type, CuisineStyle style, String description, String imagePath) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.vegetarian = vegetarian;
        this.type = type;
        this.style = style;
        this.description = description;
        this.imagePath = imagePath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", vegetarian=" + vegetarian +
                ", type=" + type +
                '}';
    }
}
