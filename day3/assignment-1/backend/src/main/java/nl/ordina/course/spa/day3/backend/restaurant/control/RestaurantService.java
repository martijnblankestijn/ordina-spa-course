package nl.ordina.course.spa.day3.backend.restaurant.control;

import nl.ordina.course.spa.day3.backend.restaurant.entity.CuisineStyle;
import nl.ordina.course.spa.day3.backend.restaurant.entity.FeaturedRestaurant;
import nl.ordina.course.spa.day3.backend.restaurant.entity.Restaurant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RestaurantService {
    private final static Map<String, Restaurant> restaurants = createRestaurants();
    private final static List<FeaturedRestaurant> featuredRestaurants = createFeaturedRestaurants();

    private static List<FeaturedRestaurant> createFeaturedRestaurants() {
        return Arrays.asList(
            new FeaturedRestaurant("cestca", "C'est Ca", "/images/restaurants/cestca/spotlight.jpg", "Alleen de kok weet wat u vanavond krijgt"),
            new FeaturedRestaurant("apicius", "Apicius", "/images/restaurants/apicius/spotlight.png", "Dineren op st(r)and"),
            new FeaturedRestaurant("blij", "Blij", "/images/restaurants/blij/spotlight.png", "Dineren aan de oevers van 'De Vecht'")
        );
    }

    private static Map<String, Restaurant> createRestaurants() {
        List<Restaurant> list = Arrays.asList(
                new Restaurant("cestca", "C'est Ca", "Je suis C\'est Ca", "Short description", "Long description", "images/restaurants/cestca/thumb.png", "images/restaurants/cestca/detail.png", CuisineStyle.FRENCH, "Utrecht", 30),
                new Restaurant("deeg", "Deeg", "Puur en simpel", "Short description", "Long description", "images/restaurants/deeg/thumb.png", "images/restaurants/deeg/detail.png", CuisineStyle.DUTCH, "Utrecht", 20),
                new Restaurant("blij", "Blij", "Gewoon Blij", "Short description", "Long description", "images/restaurants/blij/thumb.png", "images/restaurants/blij/detail.png", CuisineStyle.DUTCH, "Utrecht", 80),
                new Restaurant("apicius", "Apicius", "Dineren zoals het bedoeld is", "Korte beschrijving", "Lange beschrijving", "images/restaurants/apicius/thumb.png", "images/restaurants/apicius/detail.png", CuisineStyle.FRENCH, "Bakkum", 100)
        );

        return list.stream().collect(Collectors.toMap(Restaurant::getCode, Function.identity()));
    }

    public List<FeaturedRestaurant> getFeaturedRestaurants() {
        return featuredRestaurants;
    }

    public List<Restaurant> getAll() {
        return new ArrayList<>(restaurants.values());
    }

    public Restaurant get(String code) {
        return restaurants.get(code);
    }
}
