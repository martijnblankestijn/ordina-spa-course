package nl.ordina.course.spa.day3.backend.restaurant.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Restaurant {
    private String code;
    private String name;
    private String tagLine;
    private String shortDescription;
    private String longDescription;
    private String thumbPath;
    private String detailPath;
    private CuisineStyle style;
    private String location;
    private Integer capacity;

    public Restaurant() { }

    public Restaurant(String code, String name, String tagLine, String shortDescription, String longDescription,
                      String thumbPath, String detailPath, CuisineStyle style, String location, Integer capacity) {
        this.code = code;
        this.name = name;
        this.tagLine = tagLine;
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
        this.thumbPath = thumbPath;
        this.detailPath = detailPath;
        this.style = style;
        this.location = location;
        this.capacity = capacity;
    }

    public String getCode() {
        return code;
    }

    public Integer getCapacity() {
        return capacity;
    }
}
