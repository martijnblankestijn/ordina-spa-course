package nl.ordina.course.spa.day3.backend.restaurant.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDate;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Reservation {
    private Long Id;
    private LocalDate date;
    private Integer numberOfPersons;
    private String restaurantCode;

    public Reservation() { }

    public Reservation(Long id, LocalDate date, Integer numberOfPersons, String restaurantCode) {
        Id = id;
        this.date = date;
        this.numberOfPersons = numberOfPersons;
        this.restaurantCode = restaurantCode;
    }

    public Long getId() {
        return Id;
    }

    public String getRestaurantCode() {
        return restaurantCode;
    }

    public LocalDate getDate() {
        return date;
    }

    public Integer getNumberOfPersons() {
        return numberOfPersons;
    }
}
