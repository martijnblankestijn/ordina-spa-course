package nl.ordina.course.spa.day3.backend;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class DiningApplication extends Application {

}
