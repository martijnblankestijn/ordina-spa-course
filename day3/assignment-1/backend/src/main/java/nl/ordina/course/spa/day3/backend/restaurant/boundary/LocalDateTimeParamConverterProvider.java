package nl.ordina.course.spa.day3.backend.restaurant.boundary;

import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.invoke.MethodHandles.lookup;
import static java.util.logging.Level.INFO;
import static java.util.logging.Logger.getLogger;

/**
 * Converts JSON @QueryParam, @PathParam etc String values to Java-objects.
 */
@Provider
public class LocalDateTimeParamConverterProvider
    implements ParamConverterProvider {
  private static final Logger LOG = getLogger(lookup().lookupClass().getName());
  private static final Level LEVEL = INFO;


  @Override
  @SuppressWarnings("unchecked")
  public <T> ParamConverter<T> getConverter(
      Class<T> rawType,
      Type genericType,
      Annotation[] annotations) {
    if (LOG.isLoggable(LEVEL)) LOG.log(LEVEL, "getConverter for {0} {1} {2}", new Object[] {rawType, genericType, Arrays.toString(annotations)});

    if (rawType.equals(LocalTime.class))
      return (ParamConverter<T>) new LocalTimeConverter();
    else if (rawType.equals(LocalDate.class)) return (ParamConverter<T>) new LocalDateConverter();
    return null;
  }


  private static class LocalTimeConverter
      implements ParamConverter<LocalTime> {
    public LocalTimeConverter() {
      if (LOG.isLoggable(LEVEL)) LOG.log(LEVEL, "Constructor for type LocalTime");
    }

    @Override
    public LocalTime fromString(String value) {
      if (LOG.isLoggable(LEVEL)) LOG.log(LEVEL, "Unmarshal {0} to LocalTime", value);

      return LocalTime.parse(value);
    }

    @Override
    public String toString(LocalTime value) {
      if (LOG.isLoggable(LEVEL)) LOG.log(LEVEL, "Marshal {0} from LocalTime to String", value);

      return value.toString();
    }
  }

  private static class LocalDateConverter implements ParamConverter<LocalDate> {
    public LocalDateConverter() {
      if (LOG.isLoggable(LEVEL)) LOG.log(LEVEL, "Constructor for type LocalDate");
    }

    @Override
    public LocalDate fromString(String value) {
      if (LOG.isLoggable(LEVEL)) LOG.log(LEVEL, "Unmarshal {0} to LocalDate", value);

      return LocalDate.parse(value);
    }

    @Override
    public String toString(LocalDate value) {
      if (LOG.isLoggable(LEVEL)) LOG.log(LEVEL, "Marshal {0} from LocalDate to String", value);

      return value.toString();
    }
  }
}
