package nl.ordina.course.spa.day3.backend;

import javax.servlet.http.HttpSession;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * This filter is for demo purposes only!
 * Do not use this in production or you will introduce a serious security flaw ...
 */
@Provider
public class NeverUseInProductionFilter implements ContainerResponseFilter{

    private final boolean I_KNOW_THE_CONSEQUENCES_OF_USING_THIS_FILTER_AND_I_TAKE_FULL_RESPONSIBILITY_IN_USING_IT = true;

    @Override
    public void filter(ContainerRequestContext creq, ContainerResponseContext cres) throws IOException {
        if (I_KNOW_THE_CONSEQUENCES_OF_USING_THIS_FILTER_AND_I_TAKE_FULL_RESPONSIBILITY_IN_USING_IT) {
            cres.getHeaders().add("Access-Control-Allow-Origin", "*");
            cres.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            cres.getHeaders().add("Access-Control-Allow-Credentials", "true");
            cres.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        }
    }
}
