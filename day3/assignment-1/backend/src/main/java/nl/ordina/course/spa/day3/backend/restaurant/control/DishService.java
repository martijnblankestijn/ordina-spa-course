package nl.ordina.course.spa.day3.backend.restaurant.control;

import nl.ordina.course.spa.day3.backend.restaurant.entity.CourseType;
import nl.ordina.course.spa.day3.backend.restaurant.entity.CuisineStyle;
import nl.ordina.course.spa.day3.backend.restaurant.entity.Dish;

import javax.ws.rs.GET;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class DishService {
  private final static AtomicLong id = new AtomicLong();
  private final static Map<Long, Dish> dishes = createDishes();

  private static Dish createDish(String name, String amount, boolean vegetarian, CourseType type, CuisineStyle style, String description, String imagePath) {
    return new Dish(id.getAndIncrement(), name, new BigDecimal(amount), vegetarian, type, style, description, imagePath);
  }


  private static Map<Long, Dish> createDishes() {
    List<Dish> a = Arrays.asList(
        createDish("Chicken Piri Piri", "15.00", false, CourseType.MAIN, CuisineStyle.ASIAN, "Mildly hot traditional Indian chicken dish", "images/dishes/chicken_piri_piri.png"),
        createDish("Vegie Cannelloni", "12.50", true, CourseType.MAIN, CuisineStyle.ITALIAN, "One of Jamie Oliver's best vegetarian dishes ever!", "images/dishes/vegie_cannelloni.png")
//    {id: 3, name:'Creme Brulee', price: 8, vegetarian:true, course:'dessert',style:'French', img:'images/dishes/creme_brulee.png', description:'The traditional French dessert no menu can do without'},
//    {id: 4, name:'Pumpkin Soup', price: 5, vegetarian:true, course:'starter',style:'Dutch', img:'images/dishes/pumkin_soup.png', description:'Let the fall shine in! A must eat in this time of year'},
//    {id: 5, name:'Secret Burger', price: 7, vegetarian:false, course:'main', style:'Fusion', img:'images/dishes/true_burger.png', description:'If you have read Margaret Atwoods \'The year of the flood\' you know what the secret ingredient is!'}
    );

    return a.stream().collect(Collectors.toMap(Dish::getId, Function.identity()));
  }

  public List<Dish> getAllForRestaurant(String restaurantCode) {
    Objects.requireNonNull(restaurantCode, "Restaurant code is required");

    return new ArrayList<>(dishes.values());
  }

  /**
   * Creates a new dish and stores it.
   * 
   * @param restaurantCode the unique code of the restaurant
   * @param newDish the dish to be saved
   * @return the generated id
   */
  public Long create(String restaurantCode, Dish newDish) {
    Objects.requireNonNull(restaurantCode, "Restaurant code is required");
    Long id = DishService.id.getAndIncrement();
    
    newDish.setId(id);
    newDish.setImagePath("images/unknown.png");

    dishes.put(id, newDish);
    return id;
  }

  public void remove(long id) {
    dishes.remove(id);
  }

  public Dish get(long id) {
    return dishes.get(id);
  }

  public void update(Dish dish) {
    dishes.put(dish.getId(), dish);
  }
}
