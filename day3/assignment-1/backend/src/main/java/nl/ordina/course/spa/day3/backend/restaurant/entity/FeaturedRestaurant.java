package nl.ordina.course.spa.day3.backend.restaurant.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class FeaturedRestaurant {
    private String code;
    private String name;
    private String banner;
    private String promotionText;

    public FeaturedRestaurant() {}

    public FeaturedRestaurant(String code, String name, String banner, String promotionText) {
        this.code = code;
        this.name = name;
        this.banner = banner;
        this.promotionText = promotionText;
    }
}
