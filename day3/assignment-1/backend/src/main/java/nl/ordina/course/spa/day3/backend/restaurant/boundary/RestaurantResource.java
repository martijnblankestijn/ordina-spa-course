package nl.ordina.course.spa.day3.backend.restaurant.boundary;

import nl.ordina.course.spa.day3.backend.restaurant.control.ReservationService;
import nl.ordina.course.spa.day3.backend.restaurant.control.RestaurantService;
import nl.ordina.course.spa.day3.backend.restaurant.entity.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Path("restaurants")
public class RestaurantResource {
    private RestaurantService restaurantService = new RestaurantService();
    private ReservationService reservationService = new ReservationService();

    /**
     * Step 3: Show the lists of restaurants
     * Add a method to return a list of restaurants.
     * The method should be a GET and should return a JSON array
     */

    /**
     * Step 4: Show details of restaurant
     * Add a method to return the details of a single restaurant
     * The url should contain the restaurant code that should be used to do the lookup
     * The method should be a GET and should return a JSON object.
     *
     * If the restaurant is not found, return a 404
     */


    /**
     * Step 6: Make a reservation
     * Add a method to return the number of available seats for a given restaurant on a given date
     * The url should have the following structure: {restaurantCode}/{date}/reservations
     * The method should be a GET and should return the SeatsAvailable JSON object
     * Use the ReservationService to get the needed information
     *
     * Beware! One of the path parameters should be a LocalDate! The LocalDateTimeParamConverter should convert the incoming string to LocalDate!
     *
     */

    /**
     * Step 6: Make a reservation
     * Add a method that will do the actual reservation for a given restaurant on a given date
     * The url should have the following structure: {restaurantCode}/{date}/reservations
     * The caller should provide a NewReservation object in the message bode as JSON object.
     * If successful a 204 should be returned otherwise a 400
     *
     * Beware! One of the path parameters should be a LocalDate! The LocalDateTimeParamConverter should convert the incoming string to LocalDate!
     */
}
