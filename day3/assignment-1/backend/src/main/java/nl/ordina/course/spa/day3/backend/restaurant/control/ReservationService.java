package nl.ordina.course.spa.day3.backend.restaurant.control;

import nl.ordina.course.spa.day3.backend.restaurant.entity.Reservation;
import nl.ordina.course.spa.day3.backend.restaurant.entity.Restaurant;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class ReservationService {
    private final static AtomicLong id = new AtomicLong();
    private final static Map<Long, Reservation> reservations = new HashMap<>();

    private final RestaurantService restaurantService = new RestaurantService();

    public Integer availableSeatsOnDate(String restaurantCode, LocalDate date) {
        Restaurant restaurant = restaurantService.get(restaurantCode);

        return restaurant.getCapacity() - bookedSeatsOnDate(restaurant, date);
    }

    public boolean addReservation(String restaurantCode, LocalDate date, Integer numberOfPersons) {
        if (availableSeatsOnDate(restaurantCode, date) < numberOfPersons) {
            return false;
        } else {
            long newId = id.getAndIncrement();
            reservations.put(newId, new Reservation(newId, date, numberOfPersons, restaurantCode));

            return true;
        }
    }

    private Integer bookedSeatsOnDate(Restaurant restaurant, LocalDate date) {
        return reservations.values().stream()
                .filter(r -> restaurant.getCode().equals(r.getRestaurantCode()))
                .filter(r -> date.equals(r.getDate()))
                .map(Reservation::getNumberOfPersons)
                .reduce(0, (l, r) -> l + r);
    }
}
