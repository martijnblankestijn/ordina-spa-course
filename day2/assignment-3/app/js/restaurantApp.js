'use strict'

var restApp = angular.module("RestaurantApp", ["ngRoute", "ngResource"]);

restApp.config(function($routeProvider) {
    // Configure the routes
    // There should be 2 available routes
    // 1: Restaurant overview, available under '/' and the default page to go to. This should lead to template: 'partials/restaurant.html' and should use 'RestaurantsController'
    // 2: Restaurant details, available under '/:code'. This should lead to the template: 'partials/restaurant.html' and should use the 'RestaurantController'
});

// Configure two factories that return the different resources
restApp
    .factory("RestaurantsResource", function($resource) {}) // The url to fetch a list of the restaurants is /rest/restaurants.json
    .factory("RestaurantResource", function($resource) {}); // The url to fetch the details of a restaurant is /rest/restaurants/:code.json

restApp.controller("RestaurantsController", function($scope, ?) {
    // Load all the restaurants. Which resource method should you call?
}

restApp.controller("RestaurantController", function($scope, ?) {
    // Load the requested restaurant.
    // Hint: The restaurant code is part of the URL.

    // Optional:
    // It is possible to show the location of the restaurant on a (google) map.
    // This map will be visible if the googleFriendlyAddress variable is filled.
    // To show the correct address the 'googleFriendlyAddress' variable should follow this scheme:
    // $scope.googleFriendlyAddress = street + "+" + houseNumber + "+" + city;

    $scope.showMap = function() {
        return $scope.googleFriendlyAddress !== "";
    };

}
