# Prerequisite
This demo uses:
* NodeJS Package Manager (NPM)
* Bower
* Grunt
* Karma

Please make sure that you install all these programs.

# How to install
You need to do two things if you want to run the demo
1. You need to install the node_js modules by running 'npm install'
2. You need to download all the needed JavaScript modules by running 'bower install'

# How to run
After the above steps you should be able to run the application by running 'grunt serve'.
If you want to run all the Jasmine tests run 'grunt test' or 'grunt karma'
