'use strict'

var restApp = angular.module("RestaurantApp", ["ngRoute", "ngResource"]);

restApp.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "partials/restaurants.html",
            controller: "RestaurantsController"
        })
        .when("/:code", {
            templateUrl: "partials/restaurant.html",
            controller: "RestaurantController",
            resolve: {
                restaurant: function(RestaurantResource, $route){
                    return RestaurantResource.get({code: $route.current.params.code}).$promise;
                }
            }
        })
        .otherwise({
            redirectTo: '/'
        });
});

restApp
    .factory("RestaurantsResource", function($resource) {
        return $resource('/rest/restaurants.json');
    })
    .factory("RestaurantResource", function($resource) {
        return $resource('/rest/restaurants/:code.json');
    });

restApp.controller("RestaurantsController", function($scope, RestaurantsResource) {
    $scope.restaurants = RestaurantsResource.query();

    $scope.result = Resource.get(function() {
        // do something on success
    }, function() {
        // do something on error
    });
});

restApp.controller("RestaurantController", function($scope, restaurant) {
    $scope.googleFriendlyAddress = restaurant.address.street + "+" + restaurant.address.houseNumber + "+" + restaurant.address.city;

    $scope.restaurant = restaurant;

    $scope.showMap = function() {
        return $scope.googleFriendlyAddress !== "";
    };
});