'use strict'

var menuApp = angular.module("menuApp", ["ngRoute"]);

menuApp.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'partials/menu.html',
            controller: 'MenuController'
        })
        .when('/:id', {
            templateUrl: 'partials/dish.html',
            controller: 'DishController'
        })
        .otherwise({
            redirectTo: '/'
        });
});

menuApp.service("DataService", function($log) {
    $log.info("Instantiated the service");

    this.dishes = [
        {id: 1, name:'Chicken Piri Piri', price: 15, vegetarian:false, course:'main',style:'Indian', img:'images/dishes/chicken_piri_piri.png', description:'Mildly hot traditional Indian chicken dish'},
        {id: 2, name:'Vegie Cannelloni', price: 12.5, vegetarian:true, course:'main',style:'Italian', img:'images/dishes/vegie_cannelloni.png', description:'One of Jamie Oliver\'s best vegetarian dishes ever!'},
        {id: 3, name:'Creme Brulee', price: 8, vegetarian:true, course:'dessert',style:'French', img:'images/dishes/creme_brulee.png', description:'The traditional French dessert no menu can do without'},
        {id: 4, name:'Pumpkin Soup', price: 5, vegetarian:true, course:'starter',style:'Dutch', img:'images/dishes/pumkin_soup.png', description:'Let the fall shine in! A must eat in this time of year'},
        {id: 5, name:'Secret Burger', price: 7, vegetarian:false, course:'main', style:'Fusion', img:'images/dishes/true_burger.png', description:'If you have read Margaret Atwoods \'The year of the flood\' you know what the secret ingredient is!'}
    ];
})

menuApp.controller("DataController", function($scope, DataService) {
    $scope.dishes = DataService.dishes;

    $scope.select = function(id) {
        var i;
        var intId = parseInt(id);

        for (i = 0; i < $scope.dishes.length; i++) {
            if ($scope.dishes[i].id === intId) { return $scope.dishes[i]; };
        }

        return {id:-1, name:"Could not find a dish with id '" + id + "'!"};
    };
});

menuApp.controller("MenuController", function($scope, DataService) {
    $scope.totalNrOfDishes = $scope.dishes.length;

    $scope.nrOfDishesOfCourse = function(course) {
        var i;
        var nrOfCourses = 0;

        for (i = 0; i < $scope.dishes.length; i++) {
            if ($scope.dishes[i].course === course) {
                nrOfCourses++;
            }
        }

        return nrOfCourses;
    }

    $scope.statisticsVisible = false;

    $scope.toggleStatistics = function() {
        $scope.statisticsVisible = !$scope.statisticsVisible;
    }
});

menuApp.controller("DishController", function($scope, $routeParams) {
    $scope.dish = $scope.select($routeParams.id);
});