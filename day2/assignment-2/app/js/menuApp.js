'use strict'

var menuApp = ''; // Declare the angular module / application

menuApp.config(function ($routeProvider) {
    // Configure the routes
    // There should be 2 available routes
    // 1: Menu overview, available under '/' and the default page to go to. This should lead to template: 'partials/menu.html'
    //      If you have implemented Optional 2 then the Menu Overview route should also configure the 'MenuController' controller
    // 2: Dish details, available under '/:id'. This should lead to the template: 'partials/dish.html' and should use the 'DishController'
});

// This DataService provides the actual dish data. This service can be injected by Angular in different controllers
menuApp.service("DataService", function() {
    this.dishes = [
        {id: 1, name:'Chicken Piri Piri', price: 15, vegetarian:false, course:'main',style:'Indian', img:'images/dishes/chicken_piri_piri.png', description:'Mildly hot traditional Indian chicken dish'},
        {id: 2, name:'Vegie Cannelloni', price: 12.5, vegetarian:true, course:'main',style:'Italian', img:'images/dishes/vegie_cannelloni.png', description:'One of Jamie Oliver\'s best vegetarian dishes ever!'},
        {id: 3, name:'Creme Brulee', price: 8, vegetarian:true, course:'dessert',style:'French', img:'images/dishes/creme_brulee.png', description:'The traditional French dessert no menu can do without'},
        {id: 4, name:'Pumpkin Soup', price: 5, vegetarian:true, course:'starter',style:'Dutch', img:'images/dishes/pumkin_soup.png', description:'Let the fall shine in! A must eat in this time of year'},
        {id: 5, name:'Secret Burger', price: 7, vegetarian:false, course:'main', style:'Fusion', img:'images/dishes/true_burger.png', description:'If you have read Margaret Atwoods \'The year of the flood\' you know what the secret ingredient is!'}
    ];
})

// This DataController is the top level controller and contains the link to the dishes.
// It also contains a convenient function to get the details of specific dish
menuApp.controller("DataController", function($scope, DataService) {
    $scope.dishes = DataService.dishes;

    $scope.select = function(id) {
        var i;
        var intId = parseInt(id); // since 'id' comes from the 'path' it is a string, need to parse it to an int

        for (i = 0; i < $scope.dishes.length; i++) {
            if ($scope.dishes[i].id === intId) { return $scope.dishes[i]; };
        }

        return {id:-1, name:"Could not find a dish with id '" + id + "'!"};
    };
});

// Define a controller with the name 'DishController' for the Dish Details page
// This controller should select the dish details based on the :id in the URL of the page
// Hint: Inject and use the $routeParams for this

// Optional 2 & 3: Define a controller with the name 'MenuController' for the Menu Overview page
// This controller should contain the variables and functions to show the statistics
